﻿
#include "header_lab1.h"
using namespace std;

const string ifile = "TEXT";
const string my_text = "vm";
const string fch = "_FChar";
const string fb1 = "_FB_v1";
const string fb2 = "_FB_v2";

int main()
{
 /*   HWND hConsole = GetConsoleWindow();
    ShowWindow(hConsole, SW_HIDE);*/

    setlocale(LC_CTYPE, "Russian");


    string str, mytext;
    TextFormatting("vm");
////////////////////////////////
    ///////////Робота з текстом TEXT
    ReadFile(ifile, str);
    StringFormatting(str);

    FrequencyChar(str, "T" +  fch);
    FrequencyBigram_v1(str, "T" + fb1);
    FrequencyBigram_v2(str, "T" + fb2);

    ifstream file("T" + fch);

    cout << ifile + fch << ": " << H("T" + fch) << endl;
    cout << ifile + fb1 << ": " << H("T" + fb1) << endl;
    cout << ifile + fb2 << ": " << H("T" + fb2) << endl << endl;

//////////////////////////////
    /////////Робота з текстом vm (з пробілами)
    str = "";
    ReadFile(my_text, str);
    StringFormatting(str);

    FrequencyChar(str, "vs" + fch);
    FrequencyBigram_v1(str, "vs" + fb1);

    cout << "with space" << endl;
    cout << my_text + fch << ": " << H("vs" + fch) << endl;
    cout << my_text + fb1 << ": " << H("vs" + fb1) << endl << endl;
    
////////////////////////////
    ///////Робота з текстом vm (без пробілів)
    str = "";
    ReadFile(my_text, str, 0);
    StringFormatting(str);

    FrequencyChar(str, "v" + fch);
    FrequencyBigram_v1(str, "v" + fb1);

    cout << "without space" << endl;
    cout << my_text + fch << ": " << H("v" + fch) << endl;
    cout << my_text + fb1 << ": " << H("v" + fb1) << endl << endl;

    return 0;
}


