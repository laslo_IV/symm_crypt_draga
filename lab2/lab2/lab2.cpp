﻿#include "header_lab2.h"
#include <string>

string file = "test";
int r = 31;

int main()
{
    setlocale(LC_CTYPE, "Russian");
//////////////////////////////////////////////////////////////////////////
    ///////шифрування тексту ключами різної довжини
    ///////і визначення індексу відповідності для них
    string text = "";
    double k[30] = {};
    string mykey[] = { "рд", "три", "марк", "совун", "тайнаполтергейста"};
    TextFormatting("mytext");
    ReadFile("mytext", text, 0);


    cout << "\n*************************************** \n";
    cout << "\tВiдкритий текст\n\n";
    cout << "  I" << "\t:" << index(text) << "\t";
    cout << endl;


    for (int i = 0; i < 5; i++)
    {
        encrypt(text, mykey[i], "mytext" + to_string(i + 2));
    }

    for (int j = 0; j < 5; j++)
    {
        text = "";
        ReadFile("mytext" + to_string(j + 2) + "_encrypt", text, 0);


        cout << "\n\n\n*************************************** \n";
        cout << "\tШифротекст з ключом довжини " << mykey[j].length() << endl << endl;


        cout << "  I" << "\t:" << index(text)<< "\t";
     
    }

/////////////////////////////////////////////////////////////////////////////////////
    string str;
    string key[2];
    ReadFile("itest2", str, 0);
   

    cout << "\n\n\n***************************************\n";
    cout << "\t Аналiз заданого шифротексту \n\n";
    for (int i = 1; i < r; i++)
    {
        k[i - 1] = index(str, i);
        cout << "  I_" << i << "\t:" << k[i - 1] << "\t";
        int p = (k[i - 1] * 5) / k[0];
        cout << "  " << i << "\t";
        for (int j = 0; j < p; j++)
        {
            cout << "|";
        }
        cout << endl;
    }


    int length;
    cout << " \n\n\tУведiть довжину ключа\n \t";
    cin >> length;
    string* bloc = divide(str, length);
//
    cout << "\n\n\n\t Ключi\n\n";
    key[0] = identify_key(bloc, length, "v_FChar_f");
    cout << "  Ключ отриманий шляхом спiвставлення найчастiших лiтер блоку i мови:\n\t\"" << key[0] << "\"" << endl << endl;
    key[1] = identify_key(bloc, length);
    cout << "  Ключ отриманий з допомогою функцiї M(g)\n\t \"" << key[1] <<"\"" << endl << endl;
////////////////////////////////////////////////////////////////////////////
    



////////////////////////////////////////////////////////////////////////////

    cout << "\tШифротекст" <<  "\n\n";
    for (int i = 0; i < 5; i++)
    {
        cout << "  " << str.substr(i * 100, 100) << endl;
    }

    cout << "\n\n\n\n";
    cout << "\tТекст розшифрований з допомогою ключа: \"" << key[0] << "\" \n\n";
    decrypt(str, key[0], "otest2_0");
    text = "";
    ReadFile("otest2_0_decrypt", text, 0);
    for (int i = 0; i < 5; i++)
    {
        cout << "  " << text.substr(i * 100, 100) << endl;
    }


    cout << "\n\n\n\n";
    cout << "\tТекст розшифрований з допомогою ключа: \"" << key[1] << "\" \n\n";
    decrypt(str, key[1], "otest2_1");
    text = "";
    ReadFile("otest2_1_decrypt", text, 0);
    for (int i = 0; i < 5; i++)
    {
        cout << "  " << text.substr(i * 100, 100) << endl;
    }

    return 0;
}