#include "header_lab1.h"

void TextFormatting(string ipath)
{
    ifstream file(ipath);
    ofstream tmp_file("tmp_file");
    unsigned char ch;
    while (!file.eof())
    {
        ch = file.get();
        ch = tolower(ch);
        if ((ch > 0xdf && ch < 0x100) || ch == 0x20)
            tmp_file.put(ch);
    }
    file.close();
    tmp_file.close();
    remove(ipath.c_str());
    int k = rename("tmp_file", ipath.c_str());
}


void StringFormatting(string& text)
{
    for (unsigned long i = 0; i < text.length(); i++)
    {
        if ((unsigned char)text[i] > 0xdf && (unsigned char)text[i] < 0x100)
            text[i] = (char)((unsigned int)text[i] - 0xe0);
    }
}


void ReadFile(string ipath, string& text, bool with_space)
{
    ifstream ifile(ipath);
    string temp;
    string space = with_space ? " " : "";
    if (!ifile.is_open())
        return;

        while (!ifile.eof())
        {
            ifile >> temp;
            text += temp + space;
        }

    ifile.close();
}


void FrequencyChar(string& text, string opath)
{

    double* table = new(nothrow) double[TABLE_SIZE] {};
    if (!table) return;

    for (unsigned long i = 0; i < text.length(); i++)
        table[(int)text[i]]++;

    double text_length = text.length();


    for (int i = 0; i < TABLE_SIZE; i++)
        table[i] /= text_length;


    multimap<double, char> mtable;
    for (int i = 0; i < TABLE_SIZE - 1; i++)
        mtable.insert(pair<double, char>(table[i], (char)(-32 + i)));
    mtable.insert(pair<double, char>(table[TABLE_SIZE - 1], ' '));

    ofstream ofile(opath);
    if (!ofile.is_open())
        return;
    
    ofstream ofile_f(opath + "_f");
    if (!ofile_f.is_open())
        return;

    ofile << fixed << setprecision(10);
    ofile_f << fixed << setprecision(10);

    string out_str = "";

    for (pair<double, char> element : mtable)
    {
        string ch(1, element.second);
        out_str = ch + " " + to_string(element.first) + "\n" + out_str;

        if (element.first > 0)
            ofile << element.first << endl;
    }

    ofile_f << out_str;

    ofile.close();
    ofile_f.close();
}


void FrequencyBigram_v1(string& text, string opath)
{
    double** table = new(nothrow) double* [TABLE_SIZE];
    if (!table)
        return;
    for (int i = 0; i < TABLE_SIZE; i++)
    {
        table[i] = new(nothrow) double[TABLE_SIZE] {};
        if (!table[i])
            return;
    }
    

    for (unsigned long i = 0; i < text.length() - 1; i++)
        table[(int)text[i]][(int)text[i + 1]]++;

    double quantity_bigram = text.length() - 1;

    for (int i = 0; i < TABLE_SIZE; i++)
        for (int j = 0; j < TABLE_SIZE; j++)
            table[i][j] /= quantity_bigram;

    ofstream ofile(opath);
    if (!ofile.is_open())
        return;

    ofstream ofile_f(opath + "_f");
    if (!ofile_f.is_open())
        return;

    ofile << fixed << setprecision(10);
    ofile_f << fixed << setprecision(10);

    ofile_f << "\t\t   ";
    for (int i = 0; i < TABLE_SIZE - 1; i++)
        ofile_f << (char)(0xe0 + i) << "\t\t\t\t";
    ofile_f << endl;

    for (int i = 0; i < TABLE_SIZE; i++)
    { 
        if (i < TABLE_SIZE - 1)
            ofile_f << (char)(0xe0 + i) << "\t";
        else
            ofile_f << " " << "\t";

        for (int j = 0; j < TABLE_SIZE; j++)
        {
            ofile_f << table[i][j] << "\t";

            if (table[i][j] > 0)
                ofile << table[i][j] << endl;
        }
        ofile_f << "\n";
    }

    ofile.close();
    ofile_f.close();
}


void FrequencyBigram_v2(string& text, string opath)
{
    double** table = new(nothrow) double* [TABLE_SIZE];
    if (!table)
        return;
    for (int i = 0; i < TABLE_SIZE; i++)
    {
        table[i] = new(nothrow) double[TABLE_SIZE] {};
        if (!table[i])
            return;
    }

    int k = text.length() % 2;
    if (k) 
        text = text.substr(0, text.length() - 1);

    for (unsigned long i = 0; i < text.length(); i += 2)
        table[(int)text[i]][(int)text[i + 1]]++;

    double quantity_bigram = text.length() / 2;

    for (int i = 0; i < TABLE_SIZE; i++)
        for (int j = 0; j < TABLE_SIZE; j++)
            table[i][j] /= quantity_bigram;

    ofstream ofile(opath);
    if (!ofile.is_open())
        return;

    ofstream ofile_f(opath + "_f");
    if (!ofile_f.is_open())
        return;

    ofile << fixed << setprecision(10);
    ofile_f << fixed << setprecision(10);

    ofile_f << "\t\t   ";
    for (int i = 0; i < TABLE_SIZE - 1; i++)
        ofile_f << (char)(0xe0 + i) << "\t\t\t\t";
    ofile_f << endl;

    for (int i = 0; i < TABLE_SIZE; i++)
    {
        if (i < TABLE_SIZE - 1)
            ofile_f << (char)(0xe0 + i) << "\t";
        else
            ofile_f << " " << "\t";

        for (int j = 0; j < TABLE_SIZE; j++)
        {
            ofile_f << table[i][j] << "\t";

            if (table[i][j] > 0)
                ofile << table[i][j] << endl;
        }
        ofile_f << "\n";
    }

    ofile.close();
    ofile_f.close();
}


double H(string ipath)
{
    double h = 0.0, p = 0.0;
    string p_str;
    ifstream ifile(ipath);
    if (!ifile.is_open())
        return 0.0;

    ifile >> p_str;

    while (!ifile.eof())
    {
        p = strtod(p_str.c_str(), 0);

        if (p > 0)
            h -= p * log2(p);

        ifile >> p_str;
    }

    ifile.close();
    return h;
}






//int Symbol(unsigned char ch)
//{
//    if (ch > 0xdf && ch < 0x100)
//        return (int)ch - 0xe0;
//    if (ch == 0x20 || ch == 0xa)
//        return TABLE_SIZE - 1;
//    else
//        return -1;
//}

//void FrequencyNgram(unsigned int N, string& text, string opath, bool with_intersection)
//{
//    ofstream ofile(opath);
//    ofile << fixed << setprecision(10);
//    string tmp_str;
//    string storage = ".";
//
//    if (with_intersection)
//    {
//        double quantity_Ngram = text.length() - (double)N + 1;
//        for (unsigned long i = 0; i < text.length() - N + 1; i++)
//        {
//            tmp_str = text.substr(i, N);
//            double quantity = 0.0;
//            auto p = storage.find(tmp_str);
//            if (p == -1)
//            {
//                storage += tmp_str + ".";
//                size_t j = i;
//
//                for (j = text.find(tmp_str, j++); j != string::npos; j = text.find(tmp_str, j + 1))
//                    quantity++;
//            }
//            //double qwe = quantity / quantity_Ngram;
//            if (quantity)
//                ofile << quantity / quantity_Ngram << endl;
//            quantity = 0;
//        }
//    }
//    else
//    {
//        string tmp_text;
//        auto k = text.length() % N;
//
//        double quantity_Ngram = (text.length() - k) / N;
//
//        for (unsigned long i = 0; i < text.length() - k; i += N)
//        {
//            tmp_text += text.substr(i, N) + ".";
//        }
//
//
//
//        for (unsigned long i = 0; i < tmp_text.length(); i++)
//        {
//            tmp_str = tmp_text.substr(i, N);
//            if (tmp_str.find(".") != -1)
//                continue;
//
//            double quantity = 0.0;
//            auto p = storage.find(tmp_str);
//            if (p == -1)
//            {
//                storage += tmp_str + ".";
//                size_t j = i;
//
//                for (j = tmp_text.find(tmp_str, j); j != string::npos; j = tmp_text.find(tmp_str, j + 1))
//                    quantity++;
//            }
//            //double qwe = quantity / quantity_Ngram;
//            if (quantity)
//                ofile << quantity / quantity_Ngram << endl;
//            quantity = 0;
//        }
//    }
//    ofile.close();
//}

//double H_1(string ipath)
//{
//    double h = 0.0, p = 0.0;
//    string p_str;
//    ifstream ifile(ipath);
//    if (!ifile.is_open())
//        return 0.0;
//
//    for (int i = 0; i < TABLE_SIZE; i++)
//    {
//        ifile >> p_str >> p_str;
//        
//        p = strtod(p_str.c_str(), 0);
//
//        if (p > 0)
//            h -= p * log2(p);
//    }
//    ifile.close();
//    return h;
//}
//
//double H_2(string ipath)
//{
//    double h = 0.0, p = 0.0;
//    string p_str;
//    ifstream ifile(ipath);
//    if (!ifile.is_open())
//        return 0.0;
//
//    while (!ifile.eof())
//    {
//        ifile >> p_str;
//
//        p = strtod(p_str.c_str(), 0);
//
//        if (p > 0)
//            h -= p * log2(p);
//    }
//    
//    ifile.close();
//    return h;
//}
//
//void FrequencyChar_f(string ipath, string opath)
//{
//    
//    double* table = new(nothrow) double[TABLE_SIZE] {};
//    if (!table) return;
//
//    ifstream ifile(ipath);
//    if (!ifile.is_open())
//        return;
//
//    int tmp = 0;
//    double text_length = 0;
//
//    while (!ifile.eof())
//    {
//        tmp = Symbol(ifile.get());
//        if (tmp != -1)
//        {
//            text_length++;
//            table[tmp]++;
//        }
//    }
//
//    ifile.close();
//
//    for (int i = 0; i < TABLE_SIZE; i++)
//        table[i] /= text_length;
//
//    multimap<double, char> mtable;
//    for (int i = 0; i < TABLE_SIZE - 1; i++)
//        mtable.insert(pair<double, char>(table[i], (char)((int)'�' + i)));
//    mtable.insert(pair<double, char>(table[TABLE_SIZE - 1], ' '));
//
//
//    ofstream ofile(opath);
//    if (!ofile.is_open())
//        return;
//
//    for (pair<double, char> element : mtable)
//        ofile << element.second << " " << element.first << endl;
//    ofile << endl << endl;
//
//    ofile.close();
//}
//
//
//
//void FrequencyBigram_v1_f(string ipath, string opath)
//{
//    double** table = new(nothrow) double* [TABLE_SIZE];
//    if (!table)
//        return;
//    for (int i = 0; i < TABLE_SIZE; i++)
//    {
//        table[i] = new(nothrow) double[TABLE_SIZE] {};
//        if (!table[i])
//            return;
//    }
//
//    ifstream ifile(ipath);
//    if (!ifile.is_open())
//        return;
//
//    int tmp1 = -1, tmp2 = -1;
//    double quantity_bigram = 0;
//
//    while (!ifile.eof() && tmp1 == -1) 
//        tmp1 = Symbol(ifile.get());
//
//    while (!ifile.eof() && tmp2 == -1)
//        tmp2 = Symbol(ifile.get());
//
//    while (!ifile.eof())
//    {
//        table[tmp1][tmp2]++;
//        quantity_bigram++;
//        tmp1 = tmp2;
//        tmp2 = -1;
//        while (!ifile.eof() && tmp2 == -1)
//            tmp2 = Symbol(ifile.get());
//    }
//    ifile.close();
//
//    for(int i = 0; i < TABLE_SIZE; i++)
//        for (int j = 0; j < TABLE_SIZE; j++)
//            table[i][j] /= quantity_bigram;
//
//    ofstream ofile(opath);
//    if (!ofile.is_open())
//        return;
//
//    for (int i = 0; i < TABLE_SIZE; i++)
//    {
//        for (int j = 0; j < TABLE_SIZE; j++)
//            ofile << table[i][j] << "\t";
//        ofile << "\n";
//    }
//
//    ofile.close();
//}
//
//void FrequencyBigram_v2_f(string ipath, string opath)
//{
//    double** table = new(nothrow) double* [TABLE_SIZE];
//    if (!table)
//        return;
//    for (int i = 0; i < TABLE_SIZE; i++)
//    {
//        table[i] = new(nothrow) double[TABLE_SIZE] {};
//        if (!table[i])
//            return;
//    }
//
//    ifstream ifile(ipath);
//    int tmp1 = -1, tmp2 = -1;
//    double quantity_bigrams = 0;
//
//    if (!ifile.is_open())
//        return;
//
//    while (!ifile.eof())
//    {
//        while (!ifile.eof() && tmp1 == -1)
//            tmp1 = Symbol(ifile.get());
//        while (!ifile.eof() && tmp2 == -1)
//            tmp2 = Symbol(ifile.get());
//        if (ifile.eof()) break;
//        table[tmp1][tmp2]++;
//        quantity_bigrams++;
//        tmp1 = tmp2 = -1;
//    }
//
//    ifile.close();
//
//    for (int i = 0; i < TABLE_SIZE; i++)
//        for (int j = 0; j < TABLE_SIZE; j++)
//            table[i][j] /= quantity_bigrams;
//
//    ofstream ofile(opath);
//    if (!ofile.is_open())
//        return;
//
//    for (int i = 0; i < TABLE_SIZE; i++)
//    {
//        for (int j = 0; j < TABLE_SIZE; j++)
//            ofile << table[i][j] << "\t";
//        ofile << "\n";
//    }
//
//    ofile.close();
//}


