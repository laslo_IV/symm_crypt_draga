#include "header_lab2.h"


void Coincidence_method(string& text, string opath)
{
	map<int, unsigned long> key;

	for (int i = 2; i < 60; i++)
	{
		key.insert(pair<int, unsigned long> (i, 0));
		for (unsigned long j = 0; j < text.length() - i; j++)
		{
			if (text[j] == text[j + i])
			{
				key[i]++;
			}
		}
	}

	multimap<unsigned long, int> mkey;
	for (pair<int, unsigned long> element : key)
		mkey.insert(pair<unsigned long, int>(element.second, element.first));

	string str;
	for (pair<unsigned long, int> element : mkey)
		 str = to_string(element.second) + "\t" + to_string(element.first) + "\n" + str;

	ofstream ofile(opath);
	ofile << str;
	ofile.close();
}

string* divide(string& text, int num)
{
	string* result = new string[num];
	for (unsigned long i = 0; i < text.length(); i++)
		result[i % num] += text[i];
	return result;
}

string identify_key(string* bloc, int bloc_size, string ipath)
{
	ifstream ifile(ipath);

	string result;
	string y, k, x;
	for (int i = 0; i < bloc_size; i++)
	{
		ifile.clear();
		ifile.seekg(0, ios_base::beg);
		y = max_letter(bloc[i]);
		ifile >> x;

		result.push_back((char) ((( (int)y[0] - (int)x[0] + 64) % 32) -32) );
	}
	ifile.close();
	return result;
}

string identify_key(string* bloc, int bloc_size)
{
	string result = "";
	for (int i = 0; i < bloc_size; i++)
	{
		result.push_back(M(bloc[i], "v_FChar_f"));
	}

	return result;
}

char max_letter(string& text)
{
	double* table = new double [TABLE_SIZE - 1] {};



	for (unsigned long i = 0; i < text.length(); i++)
	{
		table[(int)text[i] - (int)'�']++;
	}

	multimap<unsigned int, char> mtable;
	for (int i = 0; i < TABLE_SIZE - 1; i++)
		mtable.insert(pair<unsigned int, char>(table[i], (char)(-32 + i)));

	map<unsigned int, char> ::iterator cur;

	cur = mtable.end();
	return (--cur)->second;
}

void encrypt(string& text, string key, string opath)
{
	string cipher_text;

	for (unsigned long i = 0; i < text.length(); i++)
	{
		cipher_text.push_back((char)((((int)text[i] + (int)key[i % key.length()] + 64) % 32) - 32));
	}

	ofstream ofile(opath + "_encrypt");
	ofile << cipher_text;
}

void decrypt(string& text, string key, string opath)
{
	string plain_text;

	for (unsigned long i = 0; i < text.length(); i++)
	{
		plain_text.push_back((char)((((int)text[i] - (int)key[i % key.length()] + 128) % 32) - 32));
	}

	ofstream ofile(opath + "_decrypt");
	ofile << plain_text;
}

void Num_letter(string& text, map<char, unsigned int>& result)
{
	double* table = new double[TABLE_SIZE - 1]{};

	for (unsigned long i = 0; i < text.length(); i++)
	{
		table[(int)text[i] - (int)'�']++;
	}

	for (int i = 0; i < TABLE_SIZE - 1; i++)
		result.insert(pair<char, unsigned int>((char)(-32 + i), table[i]));
}

double index(string text)
{
	map<char, unsigned int>num_letter;

	double n = (double)text.length() * (double)(text.length() -1);

	Num_letter(text, num_letter);
	double result = 0.0;
	for (pair<char, unsigned int> element : num_letter)
		result += (double)element.second * (double)(element.second - 1);
	result = result / n;

	return result;
}

double index(string text, int r)
{
	string* bloc = divide(text, r);
	
		return index(bloc[0]);
}

char M(string& text, string ipath)
{
	string x = "";
	map<char, unsigned int> num_letter;

	ifstream ifile(ipath);
	map<char, double> fr;
	string ch, frequency;
	char result = ' ';

	while (!ifile.eof())
	{
		ifile >> ch >> frequency;
		fr.insert(pair<char, double>(ch[0], strtod(frequency.c_str(), 0)));
	}
	
	double sum = 0.0, max = 0.0;
	Num_letter(text, num_letter);
	for (int i = (int)'�'; i < (int)'�' +1; i++)
	{
		int k = 0;
		for (int j = (int)'�'; j < (int)'�' +1; j++)
		{
			ifile.clear();
			ifile.seekg(0, ios_base::beg);
			ifile >> x;

			while (x[0] != (char)j)
			{
				ifile >> x >> x;
			}

			ifile >> x;
			if (!((i + j) % 33)) k = -32;
			sum += strtod(x.c_str(), 0) * num_letter.find((char)((i + j + k) % 33 ))->second;
		}
		if (sum > max)
		{
			max = sum;
			result = (char)(i +1) ;
			if (!(i + 1)) result = 'a';
		}
		sum = 0;
	}

	return result;
}


