﻿#include "header_lab1.h"

const string ifile = "TEXT";
const string my_text = "vm";
const string fch = "_FChar";
const string fb1 = "_FB_v1";
const string fb2 = "_FB_v2";

int main()
{
    setlocale(LC_CTYPE, "Russian");

    string str, mytext;
    TextFormatting("vm");

///////////////////////////////
    ////////
    cout << "\n\n  Оцiнка надлишковостi з допомогою cool_pink_program\n\n";
    cout << "****************************************************\n";
    cout << "****************************************************\n\n";
    cout << "  H = 5\n";
    double h = 5;
    cout << "  2.3416 < H^10 < 3.1903: \t R: " << 1 - ((2.3416 + 3.1903) / 2) / h << endl;
    cout << "  1.5376 < H^20 < 2.3398: \t R: " << 1 - ((1.5376 + 2.3398) / 2) / h << endl;
    cout << "  1.2461 < H^30 < 2.0158: \t R: " << 1 - ((1.2461 + 2.0158) / 2) / h << endl << endl;

///////////////////////////////
    ////////Робота з текстом vm (з пробілами)
    str = "";
    ReadFile(my_text, str);
    StringFormatting(str);

    FrequencyChar(str, "vs" + fch);
    FrequencyBigram_v1(str, "vs" + fb1);
    FrequencyBigram_v2(str, "vs" + fb2);
    cout << "  ****************************************************\n";
    cout << "  Текст з пробiлами\n\n";
    cout << "  H_1 = " << H("vs" + fch) << endl;
    h = H("vs" + fch);
    cout << "  2.3416 < H^10 < 3.1903: \t R: " << 1 - ((2.3416 + 3.1903) / 2) / h << endl;
    cout << "  1.5376 < H^20 < 2.3398: \t R: " << 1 - ((1.5376 + 2.3398) / 2) / h << endl;
    cout << "  1.2461 < H^30 < 2.0158: \t R: " << 1 - ((1.2461 + 2.0158) / 2) / h << endl << endl;

    cout << "  (з перетином)\n";
    cout << "  H_2 = " << H("vs" + fb1) / 2 << endl ;
    h = H("vs" + fb1) / 2;
    cout << "  2.3416 < H^10 < 3.1903: \t R: " << 1 - ((2.3416 + 3.1903) / 2) / h << endl;
    cout << "  1.5376 < H^20 < 2.3398: \t R: " << 1 - ((1.5376 + 2.3398) / 2) / h << endl;
    cout << "  1.2461 < H^30 < 2.0158: \t R: " << 1 - ((1.2461 + 2.0158) / 2) / h << endl << endl;

    cout << "  (без перетину)\n";
    cout << "  H_2 = " << H("vs" + fb2) / 2 << endl;
    H("vs" + fb2) / 2;
    cout << "  2.3416 < H^10 < 3.1903: \t R: " << 1 - ((2.3416 + 3.1903) / 2) / h << endl;
    cout << "  1.5376 < H^20 < 2.3398: \t R: " << 1 - ((1.5376 + 2.3398) / 2) / h << endl;
    cout << "  1.2461 < H^30 < 2.0158: \t R: " << 1 - ((1.2461 + 2.0158) / 2) / h << endl << endl;
    
///////////////////////////////
    ////////Робота з текстом vm (без пробілів)
    str = "";
    ReadFile(my_text, str, 0);
    StringFormatting(str);

    FrequencyChar(str, "v" + fch);
    FrequencyBigram_v1(str, "v" + fb1);
    FrequencyBigram_v2(str, "v" + fb2);

    cout << "****************************************************\n";
    cout << "  Текст без пробiлiв\n\n";
    cout << "  H_1" << ": " << H("v" + fch) << endl;
    h = H("v" + fch);
    cout << "  2.3416 < H^10 < 3.1903: \t R: " << 1 - ((2.3416 + 3.1903) / 2) / h << endl;
    cout << "  1.5376 < H^20 < 2.3398: \t R: " << 1 - ((1.5376 + 2.3398) / 2) / h << endl;
    cout << "  1.2461 < H^30 < 2.0158: \t R: " << 1 - ((1.2461 + 2.0158) / 2) / h << endl << endl;

    cout << "  (з перетином)\n";
    cout << "  H_2 (з перетином)"  << ": " << H("v" + fb1) / 2 << endl;
    h = H("v" + fb1);
    cout << "  2.3416 < H^10 < 3.1903: \t R: " << 1 - ((2.3416 + 3.1903) / 2) / h << endl;
    cout << "  1.5376 < H^20 < 2.3398: \t R: " << 1 - ((1.5376 + 2.3398) / 2) / h << endl;
    cout << "  1.2461 < H^30 < 2.0158: \t R: " << 1 - ((1.2461 + 2.0158) / 2) / h << endl << endl;
    
    cout << "  (без перетину)\n";
    cout << "  H_2 = " << H("v" + fb2) / 2 << endl;
    h = H("v" + fb2);
    cout << "  2.3416 < H^10 < 3.1903: \t R: " << 1 - ((2.3416 + 3.1903) / 2) / h << endl;
    cout << "  1.5376 < H^20 < 2.3398: \t R: " << 1 - ((1.5376 + 2.3398) / 2) / h << endl;
    cout << "  1.2461 < H^30 < 2.0158: \t R: " << 1 - ((1.2461 + 2.0158) / 2) / h << endl << endl;




    return 0;
}


